#include<iostream>
#include<conio.h>

using namespace std;

enum Suit
{
	DIAMOND, SPADE, CLUB, HEART
};

enum Rank
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card) {
	switch (card.rank) {
	case TWO: case THREE: case FOUR: case FIVE: case SIX: case SEVEN: case EIGHT: case NINE: case TEN: 
		cout << card.rank << " ";
		break;
	case JACK: 
		cout << "Jack ";
		break;
	case QUEEN: 
		cout << "Queen ";
		break;
	case KING: 
		cout << "King ";
		break;
	case ACE: 
		cout << "Ace ";
		break;
	}
	switch (card.suit) {
	case DIAMOND: 
		cout << "of Diamonds ";
		break;
	case SPADE: 
		cout << "of Spades ";
		break;
	case CLUB:
		cout << "of Clubs ";
		break;
	case HEART: 
		cout << "of Hearts ";
		break;
	}
}

//Returns true if one of the cards is greater than the other, and assigns each as a reference variable. Returns false if both the cards are the same value, but still assigns the two reference variables.
bool HighCard(Card card1, Card card2, Card& highCard, Card& lowCard) {
	if (card1.rank > card2.rank) {
		highCard = card1;
		lowCard = card2;
		return true;
	}
	else if (card2.rank > card1.rank) {
		highCard = card2;
		lowCard = card1;
		return true;
	}
	else if (card2.rank == card1.rank) {
		highCard = card1;
		lowCard = card2;
		return false;
	}
}

int main(){
	/**
	//Testing Framework for HighCard() method
	Card c1;
	c1.rank = ACE;
	c1.suit = SPADE;
	Card c2;
	c2.rank = ACE;
	c2.suit = HEART;
	Card high;
	Card low;
	
	if (HighCard(c1, c2, high, low)) {
		PrintCard(high);
			cout << "is higher than ";
			PrintCard(low);
			cout << ".\n";
	}
	else {
		PrintCard(c1);
		cout << "is the same value as ";
		PrintCard(c2);
		cout << ".\n";
	}
	_getch();
	/**/
}